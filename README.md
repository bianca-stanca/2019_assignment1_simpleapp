# 2019_assignment1_SimpleApp

First assignment for course Processo e Sviluppo Software (F1801Q154).  https://gitlab.com/biancastanca/2019_assignment1_simpleapp

## Group components:

* Bianca Stan 816045
* Samuele Pasi 816128
* Chiara Alini 861204
* Chantal Costantino 860621

## About the project:
  This is a simple Hello World application as suggested by the assignment
  requirements: when given a user id, it connects to MongoDB in order to
  display the number of accesses the user has made in the past. The project
  structure is set up according to python packages, in order to be able to
  release it into the PyPI infrastructure and then deploy it using pip install
  on a virtual machine.


## Progress tracker:

* [x]  Created a basic Hello World Application and a basic Test Unit in Python
* [x]  Set up the build stage of the pipeline; this installs the dependencies
       needed for the application to run.
* [x]  Set up verify stage; pylint does a static code analysis, ignoring naming
       conventions for convenience. This stage is not allowed to fail,
       since by ignoring the conventions warning the linter will only point out
       warnings, errors and most importantly fatal errors.
* [x]  Set up the unit test stage of the pipeline; this tests if the internal
       functions of the system work correctly.
* [x]  Set up the integration test stage of the pipeline; this tests if the
       UI properly communicates with the internal functions. It is only done
       on the master pipeline, since this is where all the separate units of
       the software need to be tested for proper collaboration.
* [x]  Set up the package stage: this simply uses setuptools in order to
       build the archives needed for a release in the PyPI infrastructure.
       It is only done on the master pipeline because it incorporated all the
       stages in order to created a single compressed folder.
* [x]  Set up the release stage: this uploads the package to test.pypi
       (for a real app, it would be the actual PyPI infrastructure). It's only
       done on the master pipeline in order to upload the just created package.
* [x] Set up the deploy stage - this is only done for the master pipeline, also.
      A connection to the provided VM is established, the dependencies needed
      for the application are installed or upgraded, and the application is then
      installed/upgraded via the pip architecture.
