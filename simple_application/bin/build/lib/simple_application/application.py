from simple_application.database_access import DatabaseAccess


def main():
    database = DatabaseAccess()

    user_id = input("Please insert your ID: ")
    result = database.getAccesses(user_id)

    if result != 1:
        print("The ID "+str(user_id)+" has accessed the database "
              +str(result)+" times")
    else:
        print("This is the first time the ID "+str(user_id)+ " has accessed the database.\
        Welcome, "+str(user_id)+"!")

    input("Press any key to quit")


if __name__ == "__main__":
    main()
