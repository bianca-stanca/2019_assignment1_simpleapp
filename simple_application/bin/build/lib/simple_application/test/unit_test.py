from simple_application.database_access import DatabaseAccess
import unittest
import random

class TestUnit(unittest.TestCase):

    def setUp(self):
        self.databaseAccess = database_access.DatabaseAccess()

    def test_newId(self):
        presentIds = self.databaseAccess.getAllIds()

        newId = random.randint(0, 1000)

        while newId in presentIds:
            newId = random.randint(0, 1000)

        self.assertEqual(1, self.databaseAccess.getAccesses(newId))

    def test_existingId(self):
        presentIds = self.databaseAccess.getAllIds()
        if len(presentIds) != 0:
            oldAccesses = self.databaseAccess.getAccesses(presentIds[0])

        self.assertEqual(oldAccesses+1,
        self.databaseAccess.getAccesses(presentIds[0]))

    def test_invalidId(self):
        with self.assertRaises(Exception) as error:
            self.databaseAccess.getAccesses("ciao")

        raised = error.exception
        self.assertIsInstance(raised, Exception)

if __name__ == '__main__':
    unittest.main()
