from simple_application import application as Application
from simple_application.database_access import DatabaseAccess
import unittest
import random

class TestApplication(unittest.TestCase):

    def setUp(self):
        self.databaseAccess = DatabaseAccess()

    def test_newId(self):
        presentIds = self.databaseAccess.getAllIds()

        newId = random.randint(0, 1000)

        while newId in presentIds:
            newId = random.randint(0, 1000)

        output = []
        input_values = [newId, 0]
        def mock_input(s):
            output.append(s)
            return input_values.pop(0)

        Application.input = mock_input
        Application.print = lambda s : output.append(s)

        Application.main()

        self.assertEqual(output, ["Please insert your ID: ",
        "This is the first time the ID "+str(newId)+ " has accessed the database.\
        Welcome, "+str(newId)+"!",
        "Press any key to quit"])

    def test_existingId(self):
        presentIds = self.databaseAccess.getAllIds()
        if len(presentIds) != 0:
            oldAccesses = self.databaseAccess.getAccesses(presentIds[0])
            output = []
            input_values = [presentIds[0], 0]

            def mock_input(s):
                output.append(s)
                return input_values.pop(0)

            Application.input = mock_input
            Application.print = lambda s : output.append(s)

            Application.main()

            self.assertEqual(output, ["Please insert your ID: ",
            "The ID "+str(presentIds[0])+" has accessed the database "
            +str(oldAccesses+1)+" times",
            "Press any key to quit"])

    def test_invalidId(self):
        with self.assertRaises(Exception) as error:
            output = []
            input_values = ["ciao"]

            def mock_input(s):
                output.append(s)
                return input_values.pop(0)

            Application.input = mock_input
            Application.print = lambda s : output.append(s)

            Application.main()

        raised = error.exception
        self.assertIsInstance(raised, Exception)

if __name__ == '__main__':
    unittest.main()
