from pymongo import MongoClient

class DatabaseAccess:
    """
    Class responsible for comunication between UI and backend
    """

    # Connects to the database using admin privileges

    def __init__(self):
        self.client = MongoClient("mongodb+srv://admin:ProcessoESviluppoSoftware20192020@1stassignmentpssw-c4r4n.gcp.mongodb.net/test?retryWrites=true&w=majority")
        self.db = self.client.accesses
        self.savedIds = self.db.accesses




    def getAccesses(self, user_id):
        """
        Given an id, returns the number of times the ID has accessed the database
        Throws exception if the ID is not an integer

        Parameters
        ----------
        user_id: parsable to int
            the ID for which to return the number of accesses
        Returns
        ----------
        user_accesses: int
            the number of times the user has accessed the database, including the
            current one
        """
        # Selects which collection to use


        # throw error if the provided id is not an integer
        try:
            user_id = int(user_id)
        except Exception as _:
            raise Exception("Id should be an integer!")


        # Queries the DB
        result = self.savedIds.find_one({"id": user_id})

        user_accesses = 1
        if result != None:
            # get the number of accesses for specified ID
            accesses = result["accesses"]

            # increments the number of accesses
            self.savedIds.replace_one({"id": user_id, "accesses": accesses},
            {"id": user_id, "accesses": accesses + 1})

            # returns the number of accesses
            user_accesses = accesses+1
            # print("The ID "+str(id)+" has accessed the database "
            # +str(accesses+1)+" times")

        else:
            # if this is the first time for provided ID, return 1
            self.savedIds.insert_one({"id": user_id, "accesses": 1})

        return user_accesses


    def deleteAllIds(self):
        """
        Deletes all the data stored in the database
        """
        print(self.savedIds.count_documents)
        self.savedIds.delete_many({})

    def getAllIds(self):
        """
        Returns all the IDs for which data has been recorded

        Returns
        ----------
        all_ids: list
            all the IDs recorded in the database
        """
        all_ids = self.savedIds.distinct("id")
        return all_ids
