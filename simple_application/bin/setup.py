from setuptools import setup

setup(name='SimpleApp',
      version='1.0',
      description='Simple app for didactic project',
      author='816045 816128 861204 860621',
      author_email = 'b.stan@campus.unimib.it',
      install_requires=['pymongo', 'dnspython'],
      packages = ['simple_application', 'simple_application.test'],
     )
